variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "david.emmerich@outlook.de"
}

variable "db_username" {
  description = "Username for the RDS postgres database"
}

variable "db_password" {
  description = "Password for the RDS postgres database"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}